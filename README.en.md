# RISC-V指令集实现

#### Description
RISC-V指令集是开源指令集，作为一个新兴开源的东西，我觉得未来可期，所以将开始关于RISC-V指令集的学习。将通过FPGA或CPLD。使用VHDL语言实现简单ip核。本项目由于我的水平有限，可能很久才能完成，当然在此之前我会逐渐了解内核的实现方式，将参照51，stm32，GD32等用自己的方式实现基于RISC-V的小型单片机。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
